#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileInfo>
#include <QMessageBox>
#include <QUrl>
#include <cmath>
#include <QDir>
#include <QTextStream>
#include <QApplication>
#include <QClipboard>

// http://qt.developpez.com/faq/?page=modules-qtnetwork-qhttp

// index of ex
// http://starbound.fr/wp-content/uploads/2013/06/
// problems
// http://amd.co.at/acp/13%20Track%2013.mp3
// http://hcmaslov.d-real.sci-nnov.ru/public/mp3/Muse/

// bad index of
// http://www.spiffy-entertainment.com/Zelda/midi/

/*
  Id�es:
  ajouter renommage des fichiers de sortie pour pas �craser les fichiers de m�me nom
  copier les adresses extraites dans la liste de href et la mettre en m�moire
  prendre en compte les chemins relatifs ". et .."
  */


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Botloader v0.2b");

    ui->plainTextEditUrls->setStatusTip("Urls to download with # to replace with a number.");
    ui->checkBoxAuto->setStatusTip("Choose a folder name automatically.");

    ui->lineEditFilterIndexof->setStatusTip("Use a regex for filter links.");
    ui->pushButtonSort->setStatusTip("Apply the filter.");

    completed = true;
    current = last = 0;

    connect(&http, SIGNAL(done(bool)), this, SLOT(httpdone()));
    connect(&http2, SIGNAL(done(bool)), this, SLOT(httpdone2()));
    connect(&http3, SIGNAL(done(bool)), this, SLOT(httpdone3()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

QString MainWindow::getUrl()
{
    QString ret = list.at(listIndex);
    QString num = QString::number(current);

    if(ui->completZeros->isChecked())
    {
        QString t = QString::number(ui->spinBoxLast->value());
        while(num.length() < t.length())
            num = "0" + num;
    }
    ret.replace( ret.indexOf('#'), 1, num);
    return ret;
}

void MainWindow::downloadFile()
{
    QUrl url = QUrl::fromUserInput(getUrl());

    QFileInfo fileInfo(url.path());
    //QString strhost = url.encodedHost();
    QString filename = fileInfo.fileName();

    if(!filename.isEmpty())
    {
        file.setFileName(folder + "/" + filename);
        http.setHost(url.host(), url.port(80));
        http.get(url.path(), &file);
    }
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Fail to open the file...", QMessageBox::Ok);
        stopped = true;
        done(false);
        file.close();
    }
    else
    {
        file.write(http.readAll());
        http.close();
    }
}

void MainWindow::httpdone()
{
    file.close();

    int v = 100 - 100 * (float)(last - current)/(last - ui->spinBoxFirt->value());
    ui->progressBar->setValue( 100 - 100 * (float)(list.size() - listIndex)/list.size() + v/list.size() );

    ui->textLog->appendPlainText(getUrl());
    current += ui->spinBoxStep->value();

    if(current <= last)
    {
        downloadFile();
    }
    else if(listIndex < list.size()-1)
    {
        listIndex++;
        last = ui->spinBoxLast->value();
        current = ui->spinBoxFirt->value();
        createFolder();
        downloadFile();
    }
    else
    {
        done();
    }
}

void MainWindow::done(bool ok) {
    completed = true;
    if(!stopped) ui->progressBar->setValue(100);
    if(ok) QMessageBox::warning(this, "Warning", "Download Done!", QMessageBox::Ok);
    ui->plainTextEditUrls->setEnabled(true);
    ui->spinBoxFirt->setEnabled(true);
    ui->spinBoxLast->setEnabled(true);
    ui->spinBoxStep->setEnabled(true);
    ui->pushButton->setEnabled(true);
    ui->completZeros->setEnabled(true);
    ui->checkBoxAuto->setEnabled(true);
    ui->tab_2->setEnabled(true);
}

void MainWindow::on_pushButton_clicked()
{
    if(!completed) return;
    if(ui->plainTextEditUrls->toPlainText().isEmpty()) return;

    // Create url list
    listIndex = 0;
    list = ui->plainTextEditUrls->toPlainText().split('\n');

    createFolder();

    last = ui->spinBoxLast->value();
    current = ui->spinBoxFirt->value();

    completed = false;
    stopped = false;
    ui->plainTextEditUrls->setEnabled(false);
    ui->spinBoxFirt->setEnabled(false);
    ui->spinBoxLast->setEnabled(false);
    ui->spinBoxStep->setEnabled(false);
    ui->pushButton->setEnabled(false);
    ui->completZeros->setEnabled(false);
    ui->checkBoxAuto->setEnabled(false);
    ui->tab_2->setEnabled(false);

    ui->progressBar->setValue(0);

    downloadFile();
}

void MainWindow::createFolder()
{
    // Get folder name
    if(!ui->checkBoxAuto->isChecked())
    {
        folder = ui->lineEditFolder->text();
    }
    else
    {
        QString url = list[listIndex];
        int pos1=0, pos2=0;
        for(int i=url.length()-1; i>=0; i--) {
            if(url[i] == '/') {
                if(pos1 == 0) pos1 = i;
                else if(pos2 == 0) {
                    pos2 = i;
                    break;
                }
            }
        }
        folder = url.mid(pos2 + 1, pos1 - pos2 - 1);
    }

    if(folder.isEmpty())
        folder = "out";

    // Create folder
    if( !QDir(folder).exists() )
        QDir().mkpath(folder);
}

void MainWindow::on_pushButtonStop_clicked()
{
    last = current - 1;
    stopped = true;
    http.abort();
}

void MainWindow::on_checkBoxAuto_clicked()
{
    ui->lineEditFolder->setEnabled( !ui->checkBoxAuto->isChecked() );
}

// Index of
void MainWindow::on_pushButtonScanFiles_clicked()
{
    urlIndexof = ui->lineEditUrlIndexof->text();
    if(urlIndexof.isEmpty()) return;

    QUrl url = QUrl::fromUserInput(urlIndexof);

    http2.setHost(url.host(), url.port(80));
    http2.get(url.path(), &file);

    file.setFileName("temp");
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Fail to open temporary file...", QMessageBox::Ok);
        file.close();
    }
    else
    {
        file.write(http2.readAll());
        http2.close();
    }

    if(urlIndexof[urlIndexof.length()-1] != '/')
        urlIndexof += '/';

    listDL.clear();
    list.clear();
    ui->listWidget->clear();
    ui->progressBarIndexof->setValue(0);
    ui->pushButtonStartIndexof->setEnabled(false);
    ui->pushButtonSort->setEnabled(true);

    ui->tab->setEnabled(false);
}

void MainWindow::httpdone2()
{
    file.close();
    //QMessageBox::warning(this, "Warning", "TMY");

    ui->tab->setEnabled(true);

    QFile f("temp");
    if(!f.open(QIODevice::ReadOnly))
        QMessageBox::warning(this, "Error", f.errorString());

    QTextStream in(&f);
    QString str = in.readAll();
    f.close();

    // Scan HTML

    int pos = 0;
    while(true) {
        int t = str.indexOf("href=\"", pos);
        if(t == -1)
            break;
        pos = t;
        QString s = str.mid(pos+6, str.indexOf('"', pos+6) - pos - 6);
        if( s.length() && s[0] != '?' && s[s.length()-1] != '/' && s[0] != '#')
            list.push_back(s);
        pos += s.length();
    }

    list.sort();
    for(int i=0; i<list.size(); i++)
        ui->listWidget->addItem(list.at(i));
    listDL = list;

    ui->pushButtonSort->setEnabled(true);
    ui->lineEditFilterIndexof->setEnabled(true);
    ui->pushButtonStartIndexof->setEnabled(true);
}



void MainWindow::on_pushButtonSort_clicked()
{
    ui->listWidget->clear();
    listDL.clear();

    // Apply filter
    bool filter = !ui->lineEditFilterIndexof->text().isEmpty();
    QRegExp regexp(ui->lineEditFilterIndexof->text());

    for(int i=0; i<list.size(); i++)
    {
        if(filter)
        {
            if(list.at(i).contains(regexp)) {
                ui->listWidget->addItem(list.at(i));
                listDL.push_back(list.at(i));
            }
        }
        else
        {
            ui->listWidget->addItem(list.at(i));
            listDL.push_back(list.at(i));
        }
    }
    list.sort();
}

void MainWindow::on_pushButtonStartIndexof_clicked()
{
    current = 0;
    stopped = false;

    if(list.size() == 0)
        return;

    ui->lineEditFilterIndexof->setEnabled(false);
    ui->pushButtonStopIndexof->setEnabled(true);
    ui->tab->setEnabled(false);
    downloadFile2();
}

void MainWindow::downloadFile2()
{
    QString urlStr = urlIndexof + listDL[current];

    if(listDL[current].mid(0, 7) == "http://")
        urlStr = listDL[current];


    QUrl url = QUrl::fromUserInput(urlStr);
    QFileInfo fileInfo(url.path());

    QDir().mkdir("href_out");
    file.setFileName("href_out/"+fileInfo.fileName());

    http3.setHost(url.host(), url.port(80));
    http3.get(url.path(), &file);

    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Fail to open the file...", QMessageBox::Ok);
        stopped = true;
        file.close();
    }
    else
    {
        file.write(http.readAll());
    }
    http3.close();
}

void MainWindow::httpdone3()
{
    file.close();

    QString urlStr = urlIndexof + listDL[current];

    if(listDL[current].mid(0, 7) == "http://")
        urlStr = listDL[current];

    ui->plainTextEditLog2->appendPlainText(urlStr);
    ui->progressBarIndexof->setValue(100 * current / listDL.size());

    if(current < listDL.size()-1 && !stopped)
    {
        current++;
        downloadFile2();
    }
    else
    {
        if(!stopped) ui->progressBarIndexof->setValue(100);
        ui->pushButtonStopIndexof->setEnabled(false);
        QMessageBox::warning(this, "Warning", "Download Done!", QMessageBox::Ok);
        ui->tab->setEnabled(true);
    }
}

void MainWindow::on_pushButtonStopIndexof_clicked()
{
    stopped = true;
    http3.abort();
}

void MainWindow::on_pushButtonClearLog1_clicked()
{
    ui->textLog->clear();
}

void MainWindow::on_pushButtonClearLog2_clicked()
{
    ui->plainTextEditLog2->clear();
}

void MainWindow::on_pushButtonCopyLog1_clicked()
{
    QApplication::clipboard()->setText(ui->textLog->toPlainText());
}

void MainWindow::on_pushButtonCopyLog2_clicked()
{
    QApplication::clipboard()->setText(ui->plainTextEditLog2->toPlainText());
}

void MainWindow::on_pushButton_2_clicked()
{
    list = ui->plainTextEditUrls->toPlainText().split('\n');
    for(listIndex=0; listIndex<list.size(); listIndex++)
        for(current=ui->spinBoxFirt->value(); current<ui->spinBoxLast->value(); current+=ui->spinBoxStep->value())
        {
        ui->textLog->appendPlainText(getUrl());
    }
}

void MainWindow::on_pushButtonGenerate2_clicked()
{
    /*
    QList<QListWidgetItem *> items = ui->listWidget->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);
    foreach(QListWidgetItem *item, items)
    {
        QString urlStr = urlIndexof + item->text();

        if(item->text().mid(0, 7) == "http://")
            urlStr = item->text();

        ui->plainTextEditLog2->appendPlainText();
    }*/
    for(int i=0; i<listDL.size(); i++)
    {
        QString urlStr = urlIndexof + listDL[i];

        if(listDL[i].mid(0, 7) == "http://")
            urlStr = listDL[i];

        ui->plainTextEditLog2->appendPlainText(urlStr);
    }
}
