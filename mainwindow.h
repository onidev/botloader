#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHttp>
#include <QFile>
#include <QBuffer>
#include <QProgressDialog>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    QHttp http, http2, http3;
    QFile file;
    bool completed, stopped;
    int current, last, listIndex;
    QString folder;
    QStringList list, listDL;
    QString urlIndexof;
    QBuffer bufffer;

    int mode;
    enum { URL, INDEXOF };

    void createFolder();
    void done(bool ok = true);

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void downloadFile();
    void downloadFile2();
    QString getUrl();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;

private slots:
    void on_pushButtonGenerate2_clicked();
    void on_pushButton_2_clicked();
    void on_pushButtonCopyLog2_clicked();
    void on_pushButtonCopyLog1_clicked();
    void on_pushButtonClearLog2_clicked();
    void on_pushButtonClearLog1_clicked();
    void on_pushButtonStopIndexof_clicked();
    void on_pushButtonStartIndexof_clicked();
    void on_pushButtonSort_clicked();
    void on_pushButtonScanFiles_clicked();
    void on_checkBoxAuto_clicked();
    void on_pushButtonStop_clicked();
    void on_pushButton_clicked();

    void httpdone();
    void httpdone2();
    void httpdone3();
};

#endif // MAINWINDOW_H
